package com.saresh.puzzle.error;

public class InvalidPuzzleException extends RuntimeException {
    public InvalidPuzzleException(String message) {
        super(message);
    }
}
