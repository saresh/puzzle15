package com.saresh.puzzle;

import com.saresh.puzzle.listener.KeyboardListener;

import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Game {
    private static final HashMap<String, MoveDirection> keyDirectionMap = new HashMap<>();
    private static ConcurrentLinkedQueue<String> keyboardInput = new ConcurrentLinkedQueue<>();
    private static int movesTotal = 0;
    private FifteenPuzzle puzzle;

    static {
        keyDirectionMap.put("Up", MoveDirection.UP);
        keyDirectionMap.put("Down", MoveDirection.DOWN);
        keyDirectionMap.put("Left", MoveDirection.LEFT);
        keyDirectionMap.put("Right", MoveDirection.RIGHT);
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.startKeyboardListener();
        game.play();
    }

    private void startKeyboardListener(){
        Thread keyboardListener = new Thread(new KeyboardListener(), "Keyboard input listener");
        keyboardListener.setDaemon(true);
        keyboardListener.start();
    }

    private void play(){
        puzzle = new FifteenPuzzle();
//        int[] customTiles = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,0,15}; /* Test config - 1 move win*/
//        puzzle = new FifteenPuzzle(customTiles);

        System.out.println("New game started, have fun!");
        displayPuzzle();
        boolean quit = false;
        while (!puzzle.isSolved()) {
            if(!keyboardInput.isEmpty()){
                String command = keyboardInput.poll();
                if (command.equals("Escape")){
                    quit = true;
                    break;
                }
                if (keyDirectionMap.containsKey(command)){
                    MoveDirection direction = keyDirectionMap.get(command);
                    if (puzzle.move(direction)) {
                        movesTotal++;
                        displayPuzzle();
                    }
                }
            }
        }
        if (quit) {
            System.out.println("User has quit the game");
        }
        if (puzzle.isSolved()){
            System.out.printf("Congratulations, you won! Moves: %s", movesTotal);
        }
        System.exit(0);
    }

    private void displayPuzzle(){
        System.out.printf("Total moves: %s\n", movesTotal);
        System.out.println(puzzle);
    }

    public static void addKeyboardInput(String keyPressed){
        keyboardInput.add(keyPressed);
    }


}
