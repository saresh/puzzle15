package com.saresh.puzzle;

import com.saresh.puzzle.error.InvalidPuzzleException;

import java.util.*;

public class FifteenPuzzle {

    private final int side = 4;
    private final int tilesNum = side * side;
    private final Random rand = new Random();
    private int[] field = new int[tilesNum];
    private int emptyTilePos;

    public FifteenPuzzle(){
        do {
            initField();
            randomizeField();
        } while (!isSolvable());
    }

    public FifteenPuzzle(int[] customField){
        if (!isFieldValid(customField)){
            throw new InvalidPuzzleException("Puzzle should contain tiles with numbers 1-15 and one empty(=0) tile");
        }
        this.field = Arrays.copyOf(customField, customField.length);
        this.emptyTilePos = findEmptyTilePosition();
    }

    private boolean isFieldValid(int[] customField){
        if (customField.length != tilesNum){
            return false;
        }
        Set<Integer> customTilesSet = new HashSet<>();
        for (int tile : customField) {
            if (!customTilesSet.add(tile)){
                return false;
            }
        }
        for (int i = 0; i < tilesNum; i++){
            if(!customTilesSet.contains(i)) {
                return false;
            }
        }
        return true;
    }

    private int findEmptyTilePosition(){
        for (int i = 0; i < tilesNum; i++) {
            if (field[i] == 0) {
                return i;
            }
        }
        throw new InvalidPuzzleException("Empty tile not found");
    }

    private void initField(){
        for (int i = 0; i < tilesNum; i++) {
            field[i] = (i + 1) % tilesNum;
        }
        emptyTilePos = tilesNum - 1;
    }

    private void randomizeField(){
        int i = tilesNum - 1;
        while (i > 1) {
            int r = rand.nextInt(i--);
            swapTiles(r, i);
        }
    }

    private void swapTiles(int aPos, int bPos){
        int temp = field[aPos];
        field[aPos] = field[bPos];
        field[bPos] = temp;
    }

    public boolean isSolvable() {
        int inversions = 0;
        for (int i = 0; i < tilesNum - 1; i++) {
            for (int j = 0; j < i; j++) {
                if (field[j] > field[i]) {
                    inversions++;
                }
            }
        }
        return inversions % 2 == 0;
    }

    public boolean isSolved(){
        if (field[tilesNum - 1] != 0) {
            return false;
        }
        for (int i = 1; i < tilesNum; i++){
            if (field[i - 1] != i) {
                return false;
            }
        }
        return true;
    }

    public boolean move(MoveDirection direction){
        int newEmptyTilePos = emptyTilePos + direction.colChange + side * direction.rowChange;
        if (newEmptyTilePos < 0 || newEmptyTilePos > tilesNum - 1) {
            return false;
        }

        int emptyTileRow = emptyTilePos / side;
        int newEmptyTileRow = newEmptyTilePos / side;
        if (direction.rowChange == 0 && emptyTileRow != newEmptyTileRow) {
            return false;
        }

        swapTiles(emptyTilePos, newEmptyTilePos);
        emptyTilePos = newEmptyTilePos;
        return true;
    }

    public int getTileValue(int position){
        if (position < 0 || position >= tilesNum){
            return -1;
        }
        return field[position];
    }

    public int[] getField(){
        return field;
    }

    @Override
    public String toString(){
        String output = " ___________________________\n";
        for (int i = 0; i < tilesNum; i++) {
            if ((i + 1) % 4 == 1){
                output += "|      |      |      |      |\n";
            }
            String tileFormat = (field[i] == 0 ? "|      " : "|%4d  ");
            output += String.format(tileFormat, field[i]);
            if ((i + 1) % 4 == 0){
                output += "|\n|______|______|______|______|\n";
            }
        }
        return output;
    }


}
