package com.saresh.puzzle;

public enum MoveDirection {
    LEFT(0,1),
    RIGHT(0, -1),
    UP(1, 0),
    DOWN(-1, 0);

    public final int rowChange;
    public final int colChange;

    MoveDirection(int rowChange, int colChange) {
        this.rowChange = rowChange;
        this.colChange = colChange;
    }
}
