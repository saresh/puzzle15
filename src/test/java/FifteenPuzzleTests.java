import com.saresh.puzzle.FifteenPuzzle;
import com.saresh.puzzle.MoveDirection;
import com.saresh.puzzle.error.InvalidPuzzleException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FifteenPuzzleTests {

    @Test
    void emptyTileOnBottomRightAfterPuzzleCreationTest() {
        FifteenPuzzle puzzle = new FifteenPuzzle();
        assertEquals(0, puzzle.getTileValue(15), "Empty tile should be on bottom right");
    }

    @Test
    void createdPuzzleIsSolvableTest() {
        FifteenPuzzle puzzle = new FifteenPuzzle();
        assertTrue(puzzle.isSolvable(), "Automatically created puzzle should always be solvable");
    }

    @Test
    void customCreatedPuzzleShouldBeValidTest() {
        int[] customNoEmpty = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
        int[] customMultipleEmpty = {1, 2, 3, 0, 5, 6, 7, 0, 9, 10, 11, 12, 13, 14, 15, 0};
        int[] customMultipleEqual = {1, 2, 3, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0};
        int[] customNot16Tiles = {1, 2, 3, 0, 5, 6, 7, 8};

        assertThrows(InvalidPuzzleException.class, () -> new FifteenPuzzle(customNoEmpty),
                "Should be exception when custom puzzle have no empty tile");
        assertThrows(InvalidPuzzleException.class, () -> new FifteenPuzzle(customMultipleEmpty),
                "Should be exception when custom puzzle have multiple empty tiles");
        assertThrows(InvalidPuzzleException.class, () -> new FifteenPuzzle(customMultipleEqual),
                "Should be exception when custom puzzle have multiple repetitive tiles");
        assertThrows(InvalidPuzzleException.class, () -> new FifteenPuzzle(customNot16Tiles),
                "Should be exception when custom puzzle created with not 16 tiles");
    }

    @Test
    void puzzleIsSolvedCheckShoulReturnCorrectValuesTest() {
        int[] customNotSolvable = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 14, 0};
        FifteenPuzzle puzzleNotSolvable = new FifteenPuzzle(customNotSolvable);
        int[] customSolvable = {13, 12, 2, 4, 15, 6, 1, 5, 7, 3, 14, 10, 11, 9, 8, 0};
        FifteenPuzzle puzzleSolvable = new FifteenPuzzle(customSolvable);

        assertTrue(puzzleSolvable.isSolvable());
        assertFalse(puzzleNotSolvable.isSolvable());
    }

    @Test
    void puzzleShouldBeSolvedWhenTilesInOrderAndBottomRightIsEmptyTest() {
        int[] customNotSolved = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 15};
        FifteenPuzzle puzzleNotSolved = new FifteenPuzzle(customNotSolved);
        int[] customSolved = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0};
        FifteenPuzzle puzzleSolved = new FifteenPuzzle(customSolved);

        assertTrue(puzzleSolved.isSolved(),
                "Puzzle should be solved when all tiles in order and bottom right tile is empty");
        assertFalse(puzzleNotSolved.isSolved(),
                "Puzzle should not be solved until all tiles in order and bottom right tile is empty");
    }

    @Test
    void moveShouldNotChangePuzzleIfMoveIsImpossibleTest() {
        int[] customEmptyTopRight = {13, 12, 2, 0, 15, 6, 1, 4, 7, 3, 14, 5, 11, 9, 8, 10};
        FifteenPuzzle puzzle = new FifteenPuzzle(customEmptyTopRight);

        puzzle.move(MoveDirection.LEFT);
        assertArrayEquals(customEmptyTopRight, puzzle.getField(),
                "Move not possible, puzzle should remain unchanged");

        puzzle.move(MoveDirection.DOWN);
        assertArrayEquals(customEmptyTopRight, puzzle.getField(),
                "Move not possible, puzzle should remain unchanged");
    }

    @Test
    void moveShouldChangeCorrectlyPuzzleIfMoveIsValidTest() {
        int[] customTiles = {5, 14, 6, 10, 15, 1, 3, 7, 12, 8, 0, 4, 13, 2, 9, 11};
        FifteenPuzzle puzzle = new FifteenPuzzle(customTiles);

        puzzle.move(MoveDirection.RIGHT);
        int[] tilesAfterMoveRight = {5, 14, 6, 10, 15, 1, 3, 7, 12, 0, 8, 4, 13, 2, 9, 11};
        assertArrayEquals(tilesAfterMoveRight, puzzle.getField(),
                "Move is valid - puzzle should change correctly");

        puzzle.move(MoveDirection.DOWN);
        int[] tilesAfterMoveRightAndDown = {5, 14, 6, 10, 15, 0, 3, 7, 12, 1, 8, 4, 13, 2, 9, 11};
        assertArrayEquals(tilesAfterMoveRightAndDown, puzzle.getField(),
                "Move is valid - puzzle should change correctly");
    }
}
