# 15-Puzzle Game

## About
The 15-puzzle is a sliding puzzle that consists of a frame of numbered square tiles in random
order with one tile missing. Game is represented by 4x4 tiles board where 15 numbered tiles are
initially placed in random order and where 16th tile is missing. A tile can be moved to a
neighbour empty place. To succeed in the game you need to order tiles from 1 to 15, where tile
number 1 is at the top left corner and empty one is at the bottom right corner.

## Controls
**Arrow up** -> move tile to empty position on the top

**Arrow down** -> move tile to empty position on the bottom

**Arrow left** -> move tile to empty position on the left

**Arrow right** -> move tile to empty position on the right

**Esc** -> Quit the game

## Run tests
    mvn test
    
## Run program
Start main method in Game class with IDE

or

1\. Build project:
```
mvn clean compile assembly:single
```    
2\.  Run jar:
```
java -jar \[path-to-jar\]
```
